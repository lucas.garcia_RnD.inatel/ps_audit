from utils import download_files, parse_text_files, extract_files, delete_ftp_files,import_csv_to_database
import sys

# >>> TEST FLAG <<<
dld = True
to_parse = True
db_update = True

# >>> CLEANING FLAG <<<<<
clean = {'txt':True,
         'csv':True,
         'ftp':True,
         'database':False}

# >>> CREDENTIALS <<<
download_dict = {'user_1': 'ossuser',
                 'password_1': 'Changeme_123',
                 'user_sql': 'lrgsouza',
                 'password_sql': 'luc4S2020',
                 'directory': '/export/home/sysm/ftproot',
                 'hosts': ['201.69.136.103', '201.69.136.104',
                           '201.69.136.105', '201.69.136.106', '201.69.136.107', '201.69.136.108', '201.69.136.109',
                           '201.69.136.110']}

#'201.69.136.100', '201.69.136.101', '201.69.136.102',
if __name__ == '__main__':
    try:

        if dld:
            # >>> DOWNLOADING <<<
            downloaded_files_df = download_files(download_dict)
            # >> EXTRACTING <<<
            extract_files()

        # >> PARSING <<<
        if to_parse:
            parse_text_files(clean)

        if db_update:
            # >> UPLOAD SQL <<<
            import_csv_to_database(download_dict,clean)

            # >> CLEAN FTP HOSTS <<<
            if clean['ftp'] and dld:
                status = delete_ftp_files(download_dict, downloaded_files_df)

    except Exception as e:
        print(e)
        print(sys.exc_info()[0])
