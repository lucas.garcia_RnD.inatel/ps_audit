from utils import download_files, parse_text_files, extract_files, delete_ftp_files,import_csv_to_database
import sys

# >>> TEST FLAG <<<
dld = True #download and extract flag
extract = True #download and extract flag
to_parse = True #parsing flag
db_update = True #csv to database upload flag

# >>> CLEANING FLAG <<<<<
clean = {'txt': True, #remove txt files after process
         'csv': True, #remove csv old files and new files after process
         'database': False, #truncate entire dadabase before upload
         'zip':True, #remove zip file after extracted
         'ftp': True} #keep false

# >>> CREDENTIALS <<<
download_dict = {'user_1': 'ossuser',
                 'password_1': 'Changeme_123',
                 'user_sql': 'lrgsouza',
                 'password_sql': 'luc4S2020',
                 'directory': '/export/home/sysm/ftproot',
                 'hosts': ['201.69.136.103', '201.69.136.104',
                           '201.69.136.105', '201.69.136.106',
                           '201.69.136.107', '201.69.136.108',
                           '201.69.136.109', '201.69.136.110'],
                 'task_list':['MMLTASK_PS_AUDIT_V2',
                              'MMLTASK_PS_AUDIT_V3'],
                 'today_filter': False}

#'201.69.136.100', '201.69.136.101', '201.69.136.102',
if __name__ == '__main__':
    try:
        #for task in task_list:
        if dld:
            # >>> DOWNLOADING <<<
            download_files(download_dict,clean)
        if extract:
            # >> EXTRACTING <<<
            extract_files(clean)
        # >> PARSING <<<
        if to_parse:
            parse_text_files(clean)
        if db_update:
            # >> UPLOAD SQL <<<
            import_csv_to_database(download_dict,clean)
        # if clean['ftp'] and dld:
        #     # >> CLEAN FTP HOSTS <<<
        #     status = delete_ftp_files(download_dict, downloaded_files_df)

    except Exception as e:
        print(e)
        print(sys.exc_info()[0])
