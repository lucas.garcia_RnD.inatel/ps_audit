import pandas as pd
import ftplib
import os
import gzip
import shutil
from datetime import datetime
import mysql.connector as mysql
import platform
import sys
import math
from sqlalchemy import create_engine

def create_index(row):
    time = str(row['TIME']).replace('-', '_')
    time = time.replace(' ', '_')
    time = time.split(':')[0]
    ne_name = row['NE_NAME']
    resource_name = row['RESOURCE_ITEM']

    return f'{time}_{ne_name}_{resource_name}'


def ftp_connection(host, download_dict, task):
    try:
        ftp = ftplib.FTP(host)
        user = download_dict['user_1']
        password = download_dict['password_1']

        directory = download_dict['directory']
        ftp.login(user, password)
        ftp.cwd(directory)
        files = ftp.nlst()
        file_names = []
        file_directories = []

        df_ftp = pd.DataFrame(columns=['file_name', 'file_directory'])

        df_download = pd.DataFrame(columns=['file_directory', 'file_name'])

        if 'MMLTaskResult' in files:
            print(f'- Mapping files from {host}')
            directory = f'{directory}/MMLTaskResult'
            ftp.cwd(directory)
            files = ftp.nlst()
            print('1 - Has MMLTaskResult')
        else:
            print(f'No files to download from {host}')
            return ftp, df_ftp

        for current_file in files:
            file_names.append(current_file)
            current_directory = f'{directory}/{current_file}'
            file_directories.append(current_directory)

        df_ftp['file_name'] = file_names
        df_ftp['file_directory'] = file_directories
        df_ftp['final_step'] = False
        print('2 - Finish list directories')

        df_ftp = df_ftp[df_ftp['file_name'] != '.']
        df_ftp = df_ftp[df_ftp['file_name'] != '..']

        df_ftp = df_ftp[df_ftp['file_name'].str.isdigit()]

        df_download_name = []
        df_file_name = []

        for index, row in df_ftp.iterrows():

            file_path = f"{row['file_directory']}/history"

            try:
                ftp.cwd(file_path)
            except:
                df_ftp.drop(index, inplace=True)
                continue

            files = ftp.nlst()


            if any([x for x in files if task in x.upper()]):

                df_download_name.extend([f'{file_path}/{x}' for x in files if task in x.upper()])

                df_file_name.extend([x for x in files if task in x.upper()])

                df_ftp.loc[index, 'file_directory'] = f"{row['file_directory']}/output/MmlTaskResult_Download.txt.tar.gz"
                df_ftp.loc[index, 'file_name'] = "MmlTaskResult_Download.txt.tar.gz"
            else:
                ftp.cwd(directory)
                df_ftp.drop(index, inplace=True)
                continue

        df_download['file_directory'] = df_download_name
        df_download['file_name'] = df_file_name

        today = datetime.now()
        today = today.strftime('%Y%m%d')

        if df_download.empty:
            print(f'3 - No files to download on this host')
            return ftp, df_download
        else:
            print('3 - There are some files to download on this host')
            if download_dict['today_filter']:
                df_download = df_download[df_download['file_name'].str.contains(today)]
                print('4 - Filtering today files')
                if df_download.empty:
                    print(f'5 - There are no today files to download on this host')
                    return ftp, df_download
                else:
                    for index, item in enumerate(df_download['file_name']):
                        print(index, '-', item)
                    return ftp, df_download
                    print('5 - Host complete')
            else:
                print('4 - Listing files to download')
                for index, item in enumerate(df_download['file_name']):
                    print('  ',index, '-', item)
                print('5 - Host complete')
                return ftp, df_download

    except Exception as e:
        print(e)
        #print(sys.exc_info()[0])
        print(f'>>>>Error on HOST: {host}<<<<')
        try:
            ftp.close()
        except:
            return False, False


def download_files(download_dict,clean):

    print(f'>>>>Starting {download_dict["task_list"]} files download!<<<<')

    for task in download_dict['task_list']:
        print(f'>>>>Searching {task} files download!<<<<')

        hosts = download_dict['hosts']

        index_download = 0

        downloaded_files_df = pd.DataFrame(columns=['host', 'file_path'])

        dir_path = os.path.dirname(os.path.realpath(__file__))
        dir_path = os.path.join(dir_path, 'files')

        for host in hosts:

            try:
                ftp, df_ftp = ftp_connection(host, download_dict, task)

                if df_ftp.empty:
                    print('>>>>Aborting files download!<<<<')
                    ftp.close()
                    continue

                for index, row in df_ftp.iterrows():
                    file_to_download = row['file_directory']
                    file_to_download_name = row['file_name']

                    full_file_path = os.path.join(dir_path, 'zip')
                    full_file_path = os.path.join(full_file_path, file_to_download_name)
                    ftp.retrbinary("RETR " + file_to_download, open(full_file_path, 'wb').write)
                    print(f'Downloaded: {file_to_download_name}')
                    index_download += 1

                    new_dict = {'host': [host], 'file_path': [file_to_download]}
                    insert_df = pd.DataFrame(new_dict)
                    downloaded_files_df = pd.concat([downloaded_files_df, insert_df], ignore_index=True)

                ftp.close()

            except Exception as e:
                print(e)
                print(sys.exc_info()[0])
                print(f'>>>>Error on Access {host}<<<<')
                try:
                    ftp.close()
                except:
                    pass
        print(f'>>>>Finished {task} files download!<<<<<<<<')
        #apagando arquivos
        if clean['ftp']:
            delete_ftp_files(download_dict, downloaded_files_df)


    return True


def extract_files(clean):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'files')
    zip_directory = os.path.join(file_path, 'zip')
    usedzip_directory = os.path.join(file_path, 'usedzip')
    txt_directory = os.path.join(file_path, 'txt')

    zip_files = [f for f in os.listdir(zip_directory) if os.path.isfile(os.path.join(zip_directory, f))]

    print('>>>>Starting files extraction!<<<<')

    for extracting in zip_files:
        full_zip_dir = os.path.join(zip_directory, extracting)
        full_usedzip_dir = os.path.join(usedzip_directory, extracting)
        target_file_name = str(extracting.split('.txt')[0]) + '.txt'
        full_target_dir = os.path.join(txt_directory, target_file_name)
        with gzip.open(full_zip_dir, 'rb') as f_in:
            with open(full_target_dir, 'wb') as f_out:
                try:
                    shutil.copyfileobj(f_in, f_out)
                except:
                    pass
        shutil.copyfile(full_zip_dir, full_usedzip_dir)
        print(f'>>>>Copy user zip done, destination: {full_usedzip_dir}<<<<')
        if clean['zip']:
            os.remove(full_zip_dir)

    print('>>>>Finished files extraction!<<<<')

def integrity_check(lines,date):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'files')
    csv_path = os.path.join(file_path, 'csv')
    temp_path = os.path.join(file_path, 'temp.txt')
    retcode_list = []
    f_erro = False
    now = date

    #processing RETCODE lines
    count = 1
    for loop in range(0,20): #
        for x in range(0,len(lines)):
            if 'RETCODE = ' in str(lines[x]):
                if lines[x].strip() != 'RETCODE = 0  Operation Success.':
                    print(f'Error found! count: {count}')
                    count += 1
                    f_erro = True
                    #'processing file'
                    if '@' in lines[x-5]:
                        ne_name = lines[x-5].strip().split('@')[-1]
                    else:
                        ne_name = lines[x-5].strip()
                    block_to_del = [x-6,x+8]
                    command = lines[x-1].strip().split('/')[-1]
                    command = command.strip().split(':')[0]
                    retcode_list.append([now,ne_name,command,lines[x]])
                    lines = [x for i, x in enumerate(lines) if i not in range(block_to_del[0], block_to_del[1])]
                    break

    if f_erro:
        retcode_df = pd.DataFrame(retcode_list, columns=['DATE','NE_NAME','COMMAND','RETCODE'])
        csv_name = f'd_ERROR_{now}.csv'
        csv_final_path = os.path.join(csv_path, csv_name)
        print(f'>>>>>>>>Saving {csv_name} to disk!<<<<<<<<<<<<')
        #check if csv already existis
        if not os.path.isfile(csv_final_path):
            retcode_df.to_csv(csv_final_path, index=False)
        else:
            df_csv = pd.read_csv(csv_final_path, dtype = 'str', low_memory=False)
            df_final = pd.concat([df_csv, retcode_df], axis=0, ignore_index=0)
            os.remove(csv_final_path)
            df_final.to_csv(csv_final_path, index=False)
        #check if csv already existis
        #retcode_df.to_csv(csv_final_path, index=False)

    return lines

def save_temp_file(temp_path,lines):
    print('salvando arquivo temporário para análise...')
    with open(temp_path, 'w') as f:
        for item in lines:
            f.write("%s\n" % item)

def parse_text_files(clean):
    print('>>>>Starting files parse and CSV generation!<<<<')
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'files')
    csv_path = os.path.join(file_path, 'csv')
    txt_path = os.path.join(file_path, 'txt')
    error_path = os.path.join(file_path, 'error_log.txt')
    temp_path = os.path.join(file_path, 'temp.txt')

    files = [f for f in os.listdir(txt_path)]

    #apagando CSVs atuais
    if clean['csv']:
        current_csv = [f for f in os.listdir(csv_path)]
        if current_csv:
            for cur_csv in current_csv:
                os.remove(os.path.join(csv_path, cur_csv))

    for cur_file in files:
        print(f'Starting to parse: {cur_file}')
        ref_hour = 'Unknown Hour'
        # DEFINE HORA PARA SUBIR
        date = cur_file.split('.')[0]

        hour = date.split('_')[-1][:-2]
        ref_hour = f'{hour[0:2]}:00:00'
        hour = f'{hour[0:2]}:{hour[2:]}'

        date = date.split('_')[-2]
        ref_hour = date
        date = f'{date[0:4]}-{date[4:6]}-{date[6:]}'

        #ref_hour = date + ' ' + ref_hour
        file_full_path = os.path.join(txt_path, cur_file)
        try:
            file_lines = open(file_full_path, 'r')
            lines = file_lines.readlines()

            #========= verify integrity ================== "implementar tratamento de erros via RETCODE <> 0"
            print('Start integrity check!')
            lines = [x for x in lines if not x.strip().__contains__('No matching result is found')]
            lines = integrity_check(lines,date)
            print('Finish integrity check!')

            #removendo linhas indesejaveis
            lines = [x for x in lines if not x.strip() == '']
            lines = [x for x in lines if not x.strip().__contains__('.txt')]
            lines = [x for x in lines if not x.strip().__contains__('MML Command Report:')]
            lines = [x for x in lines if not x.strip().__contains__('MML Command:')]
            #lines = [x for x in lines if not x.strip().__contains__('NE Name:')]
            lines = [x for x in lines if not x.strip().__contains__('+++')]
            lines = [x for x in lines if not x.strip().__contains__('O&M')]
            lines = [x for x in lines if not x.strip().__contains__('MML Session')]
            lines = [x for x in lines if not x.strip().__contains__('RETCODE')]
            lines = [x for x in lines if not x.strip().__contains__('The result is as follows')]
            lines = [x for x in lines if not x.strip().__contains__('To be continued')]
            lines = [x for x in lines if not x.strip().__contains__('---    END')]
            lines = [x for x in lines if not x.strip().__contains__('------------------------')]
            lines = [x for x in lines if not x.strip().__contains__('Operation Success.')]
            lines = [x for x in lines if not x.strip().__contains__('MML Command Return Code:')]
            lines = [x for x in lines if not x.strip().__contains__('MML Command Result:')]
            lines = [x for x in lines if not x.strip().__contains__('reports in total')]
            lines = [x for x in lines if not x.strip() == '0']
            for index, line in enumerate(lines):
                lines[index] = line.strip()
            file_lines.close()

            #encontrando comandos no arquivo
            index_commands = [i for i, x in enumerate(lines) if x[len(x)-1:len(x)] == ';']


            #gerando dicionario de comandos/colunas
            print('gerando dicionario de colunas')
            commands_columns_dict = {}
            for x in index_commands:
                column_list = []
                cur_comm = lines[x].strip().split(':')[0]
                # comando vertical
                if '=' in str(lines[x+3]):
                    for j in range(x+3,x+31):
                        if not lines[j].strip().__contains__('Number of results'):
                            column_element = lines[j].strip().split('=')[0]
                            column_list.append(column_element.strip())
                        else:
                            break
                #comando horizontal
                else:
                    column_list = lines[x+3].strip().split('  ')
                    for index, x in enumerate(column_list):
                        column_list[index] = x.strip()

                #dict
                column_list = [x for x in column_list if x.strip()]
                column_list.insert(0, 'NE_NAME')
                column_list.insert(1, 'DATE')
                commands_columns_dict[cur_comm] = column_list

            #eliminando cabeçalhos
            print('eliminando cabeçalhos...')
            for k in commands_columns_dict:
                lines = [x for x in lines if not x.strip().__contains__(commands_columns_dict[k][2]) or x.strip().__contains__('=')]

            #reavaliando comandos
            index_commands = [i for i, x in enumerate(lines) if x[len(x)-1:len(x)] == ';']

            #verificando onde o comando muda o elemento
            block_start = [i for i, x in enumerate(lines) if x.upper().strip().__contains__('NE NAME:')]
            block_end = [i for i, x in enumerate(lines) if x.upper().strip().__contains__('RESULTS =')]



            #pc_date = datetime.now().date().strftime("%Y-%m-%d")

            #salvando arquivo temporario
            save_temp_file(temp_path,lines)

            #define dict linhas por comandos principais
            command_block = {}
            for i, command in enumerate(commands_columns_dict):
                if i+1 < len(index_commands):
                    command_block[command] = [index_commands[i],index_commands[i+1]]
                else:
                    command_block[command] = [index_commands[i],len(lines)]
            #define dict de blocos por comandos principais
            inside_blocks = {}
            for command in command_block:
                temp_list = []
                for i in range(0,len(block_end)):
                    if block_end[i] in range(command_block[command][0],command_block[command][1]):
                        temp_list.append([block_start[i]+2,block_end[i]])
                inside_blocks[command] = temp_list


            # ===============bloco cria CSV por comando================
            for command in command_block:
                print(f'>>>>>>>>Start processing {command} block<<<<<<<<<<<<')
                #define colunas do DF
                df_column = commands_columns_dict[command]
                df_list = []
                try:
                    #processa bloco a bloco dentro do bloco inteiro
                    for inner_block in inside_blocks[command]:
                        for i in range(inner_block[0],inner_block[1]):

                            #CHECANDO NOME
                            if '@' in lines[inner_block[0]-1]:
                                ne_name = lines[inner_block[0]-1].strip().split('@')[-1]
                            else:
                                ne_name = lines[inner_block[0]-1].strip()

                            # comando vertical para 1 resultado
                            if '=' in str(lines[i]) and 'NE NAME' in str(lines[i-2]).upper():
                                split_list = []
                                for v in range(inner_block[0],inner_block[1]):
                                    result = lines[v].strip().split('=')[-1]
                                    split_list.append(result.strip())
                                split_list.insert(0, date)
                                split_list.insert(0, ne_name)
                                df_list.append(split_list)
                                break

                            #comando vertical no final de blocos horizontais
                            if '=' in str(lines[i]) and 'NE NAME' not in str(lines[i-2]).upper():
                                split_list = []
                                for v in range(i,i+len(df_column)-2):
                                    result = lines[v].strip().split('=')[-1]
                                    split_list.append(result.strip())
                                split_list.insert(0, date)
                                split_list.insert(0, ne_name)
                                df_list.append(split_list)
                                break

                            #comando horizontal
                            else:
                                split_list = []
                                split_list = lines[i].strip().split('  ')
                                split_list = [x.strip() for x in split_list if not x.strip() == '']
                                split_list.insert(0, date)
                                split_list.insert(0, ne_name)
                                df_list.append(split_list)

                    #gerando df com todos os resultados do corrente comando
                    response_df = pd.DataFrame(df_list, columns=df_column)

                    #formatando colunas
                    list_colums = list(response_df.columns)
                    for column in list_colums:
                        if not column == 'DATE':
                            response_df[column] = response_df[column].astype('str')

                    now = datetime.now()
                    now = now.strftime('%Y%m%d_%H')

                    table_name = command.split(' ')[-1]

                    csv_name = f'd_{table_name}_{now}.csv'
                    csv_final_path = os.path.join(csv_path, csv_name)
                    print(f'>>>>>>>>Saving {csv_name} to disk!<<<<<<<<<<<<')
                    #check if csv already existis
                    if not os.path.isfile(csv_final_path):
                        response_df.to_csv(csv_final_path, index=False)
                    else:
                        df_csv = pd.read_csv(csv_final_path, dtype = 'str', low_memory=False)
                        df_final = pd.concat([df_csv, response_df], axis=0, ignore_index=0)
                        os.remove(csv_final_path)
                        df_final.to_csv(csv_final_path, index=False)
                    #check if csv already existis
                    #response_df.to_csv(csv_final_path, index=False)
                except Exception as e:
                    print(e)
                    continue

        except Exception as e:
            print(e)
            error_file = open(error_path, 'a')
            error_file.write(f'Error on {cur_file} at {ref_hour}:\n'
                             f'{e}\n'
                             f'\n ######################################## \n')
        if clean['txt']:
            os.remove(file_full_path)

    print('>>>>Finished CSV generation!<<<<')

def import_csv_to_database(download_dict,clean):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'files')
    csv_path = os.path.join(file_path, 'csv')

    print('>>>>Starting Database importing!<<<<')

    db = mysql.connect(
        host="172.29.200.126",
        user=download_dict['user_sql'],
        passwd=download_dict['password_sql'],
        allow_local_infile=True
    )
    cursor = db.cursor()


    command_type_dir = f'{csv_path}/'

    all_commands = os.listdir(command_type_dir)

    #define dict para tipos
    type_dict = {'-IMSVMH-ATS':'ATS',
                 '-IMSVMH-SBC':'CloudSE2980',
                 '-IMSVMH-CSC':'CSCF',
                 '_UDCHUA_HSS':'HSS9860',
                 '_UDCHUA_SBC':'SE2900',
                 '_DRAHUA_SPS':'SPSV3',
                 '_DEAHUA_SPS':'SPSV3',
                 '_EPCVMH_UPF':'UDG',
                 '_UDCVMH_HSS':'UDM',
                 '_GPRHUA_GGS':'UGW',
                 '_EPCVMH_NCG':'UNC',
                 '_EPCVMH_AMF':'UNC',
                 '_EPCVMH_SMF':'UNC',
                 '_GPRHUA_SGS':'USN',
                 '_EPCVMH_CGW':'vCG',
                 '_EPCVMH_EPG':'vePDG',
                 '_EPCVMH_UGW':'vUGW',
                 '_EPCVMH_UCG':'vUGW',
                 '_EPCVMH_UDG':'vUGW',
                 '_EPCVMH_USN':'vUSN'}

    for current_command in all_commands:

        command = current_command.replace('.csv', '')
        command = command.split('_')[0]+'_'+command.split('_')[1]
        print(f'>>>>>>>>Working on {command} DataFrame<<<<<<<<')

        try:
            # CHECK IF TABLE EXISTS
            current_mo_dir = f'{command_type_dir}{current_command}'
            df_csv = pd.read_csv(current_mo_dir, dtype = 'str', low_memory=False)
            list_colums = list(df_csv.columns)
            # DEFINE DEFAULT DATE
            now = df_csv['DATE'].iloc[0]

            for type_text in type_dict:
                d_db = type_dict[type_text]
                #filtrando CSV por tipo, se verdadeiro executa
                filtered_df = df_csv[df_csv['NE_NAME'].str.contains(type_text)]
                if not filtered_df.empty:
                    print(f'>>>>>>>>Working on {d_db} Database<<<<<<<<')
                    try:

                        #criando tabela se o comando for novo
                        sql_table_creation = f'CREATE TABLE IF NOT EXISTS {d_db}.{command} ('
                        for column in list_colums:
                            if not column == 'DATE':
                                column = column.replace('.','')
                                column = column.replace('/','_')
                                column = column.replace('-','_')
                                sql_insert_col = f"{column.replace(' ','_')} varchar(100) DEFAULT '-', "
                                sql_table_creation = sql_table_creation + sql_insert_col
                            else:
                                column = column.replace('.','')
                                column = column.replace('-','_')
                                sql_insert_col = f"{column.replace(' ','_')} date DEFAULT NULL, "
                                sql_table_creation = sql_table_creation + sql_insert_col

                        sql_table_creation = sql_table_creation[:-2] + ');'
                        cursor.execute(sql_table_creation)
                        db.commit()

                        #cleaning table
                        if clean['database']:
                            print(f'Cleaning {d_db}.{command}...')
                            sql = f'TRUNCATE {d_db}.{command};'
                            cursor.execute(sql)
                            db.commit()

                        #formatando colunas
                        list_colums = list(filtered_df.columns)
                        for column in list_colums:
                            if not column == 'DATE':
                                filtered_df[column] = filtered_df[column].astype('str')

                        #SALVANDO ARQUIVO TEMPORARIO PARA SUBIR NO BANCO
                        csv_name = f'{d_db}.{command}.csv'
                        csv_final_path = os.path.join(csv_path, csv_name)
                        filtered_df.to_csv(csv_final_path, index=False)

                        #subindo CSV na tabela
                        csv_final_path = csv_final_path.replace('\\', '/')

                        cur_platform = platform.system()
                        print(f'Loading {d_db}.{command}...')
                        if cur_platform.upper() == 'WINDOWS':
                            sql = f'LOAD DATA LOCAL INFILE "{csv_final_path}" REPLACE INTO TABLE {d_db}.{command}\n' \
                                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                                  f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
                        else:
                            sql = f'LOAD DATA LOCAL INFILE "{csv_final_path}" REPLACE INTO TABLE {d_db}.{command}\n' \
                                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                                  f'LINES TERMINATED BY \'\n\' ignore 1 lines;'
                        cursor.execute(sql)
                        db.commit()

                        print(f'Done: {d_db}.{command}')
                        os.remove(csv_final_path)

                    except Exception as e:
                        print(f'exception: ', e)
                        try:
                            if 'DATA TOO LONG FOR COLUMN' in e.msg.upper():
                                print(f'Start fixing the column len')
                                all_columns = list(df_csv.columns)
                                for check_col in all_columns:
                                    df_col_len = int(df_csv[check_col].astype(str).str.encode(encoding='utf-8').str.len().max())
                                    df_col_len = roundup(df_col_len)
                                    if df_col_len > 100:
                                        sql = f"ALTER TABLE {d_db}.{command} \n" \
                                              f"CHANGE COLUMN `{check_col}` `{check_col}`" \
                                              f" VARCHAR({df_col_len}) DEFAULT '-';"

                                        cursor.execute(sql)
                                db.commit()
                                cur_platform = platform.system()
                                print(f'Loading {d_db}.{command}...')
                                if cur_platform.upper() == 'WINDOWS':
                                    sql = f'LOAD DATA LOCAL INFILE "{csv_final_path}" REPLACE INTO TABLE {d_db}.{command}\n' \
                                          f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                                          f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
                                else:
                                    sql = f'LOAD DATA LOCAL INFILE "{csv_final_path}" REPLACE INTO TABLE {d_db}.{command}\n' \
                                          f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                                          f'LINES TERMINATED BY \'\n\' ignore 1 lines;'

                                cursor.execute(sql)
                                db.commit()

                                print(f'Done: {command}')
                                #deletando arquivo filho
                                os.remove(csv_final_path)
                        except Exception as e:
                            print(f'exception: ', e)
                            os.remove(csv_final_path)

                else:
                    continue

            #deletando arquivo mãe
            if clean['csv']:
                os.remove(current_mo_dir)

        except Exception as e:
            print(f'exception: ', e)
            print(f'ERROR: {command}')

    print('>>>>Finished Database importing!<<<<')


def delete_ftp_files(download_dict, downloaded_files_df):
    print('>>>>Deleting FTP files!<<<<')
    if downloaded_files_df.empty:
        return 'NOK'

    hosts = downloaded_files_df['host'].unique().tolist()

    for cur_host in hosts:
        print(f'- Deleting files on {cur_host}')
        cur_df = downloaded_files_df[downloaded_files_df['host'] == cur_host]

        ftp = ftplib.FTP(cur_host)

        user = download_dict['user_1']
        password = download_dict['password_1']

        try:
            ftp.login(user, password)
        except:
            try:
                ftp.close()
            except:
                pass

            continue

        files_to_delete = cur_df['file_path'].tolist()

        for deleting in files_to_delete:
            try:
                ftp.delete(deleting)
            except:
                pass

        ftp.close()

    print('>>>>Finished files deletion!<<<<')
    return 'OK'

def roundup(x):
    return int(math.ceil(x / 100.0)) * 100